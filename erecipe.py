import click

from app import app, db
from app.models import Recipe, Ingredients
from init_database import update_recipes

@app.shell_context_processor
def make_shell_context():
    return {'db': db, 'Recipe': Recipe, 'Ingredients': Ingredients}

@app.cli.command()
def initdb():
    update_recipes()
    click.echo('Database initialized')

@app.cli.command()
def list_all_recipes():
    recipes = Recipe.query.all()
    for recipe in recipes:
        click.echo(f'Recipe: {recipe.name}')
        click.echo('Ingredients:')
        for ingredient in recipe.ingredients:
            click.echo(f'{ingredient.amount} {ingredient.unit} of {ingredient.name}')
        click.echo(f'Instructions: {recipe.instructions}')
        click.echo('\n')


