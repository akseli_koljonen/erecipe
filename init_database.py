from app import db
from app.models import Recipe, Ingredients
import json

def update_recipes():
    recipes_entries = []
    ingredient_entries = []
    with open('recipes.json') as f:
        data = json.load(f)
        for recipes in data:
            recipe_entry = Recipe(name=recipes['name'],
                                  instructions=recipes['instructions']
                                  )
            for ingredient in recipes["ingredients"]:
                ingredien_entry = Ingredients(name=ingredient['name'],
                                              amount=ingredient['amount'],
                                              unit=ingredient['unit'],
                                              recipe=recipe_entry
                                            )
                ingredient_entries.append(ingredien_entry)
            recipes_entries.append(recipe_entry)
    try:
        db.session.add_all(ingredient_entries)
        db.session.add_all(recipes_entries)
        db.session.commit()
    except:
        return