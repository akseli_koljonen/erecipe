# eRecipe

## Table of contents
* [General info](#general-info)
* [Technologies](#technologies)
* [Setup](#setup)

## General info
Pre-assignment for Gofore interview
	
## Technologies
Project is created with:
* Python 3.9
* Pipenv
* Flask
* Sqlite
	
## Setup
To run this project, you need to install Python 3.9 and Pipenv

```
pip install pipenv
```

To install all the requirements run:

```
pipenv install
```

To run the project, first initialize the database

```
pipenv run flask initdb
```

To print all the recipes

```
pipenv run flask list-all-recipes
```

To open the webclient run

```
pipenv run flask run
```