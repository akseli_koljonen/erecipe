from app import db
from datetime import datetime

import json

class Recipe(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), index=True, unique=True)
    ingredients = db.relationship('Ingredients', backref='recipe', lazy='dynamic')
    instructions = db.Column(db.String(128))

    def __repr__(self):
        return '<Recipe {}>'.format(self.name)

class Ingredients(db.Model):
    id = db.Column(db.Integer, primary_key=True) 
    name = db.Column(db.String(64))
    amount = db.Column(db.Integer)
    unit = db.Column(db.String(64))
    timestamp = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    recipe_id = db.Column(db.Integer, db.ForeignKey('recipe.id'))

    def __repr__(self):
        return '<Ingredient {}>'.format(self.name)

def update_recipes():
    recipes_entries = []
    ingredient_entries = []
    with open('recipes.json') as f:
        data = json.load(f)
        for recipes in data:
            recipe_entry = Recipe(name=recipes['name'],
                                  instructions=recipes['instructions']
                                  )
            for ingredient in recipes["ingredients"]:
                ingredien_entry = Ingredients(name=ingredient['name'],
                                              amount=ingredient['amount'],
                                              unit=ingredient['unit'],
                                              recipe=recipe_entry
                                            )
                ingredient_entries.append(ingredien_entry)
            recipes_entries.append(recipe_entry)
    try:
        db.session.add_all(ingredient_entries)
        db.session.add_all(recipes_entries)
        db.session.commit()
    except:
        return
