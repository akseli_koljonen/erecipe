from flask_wtf import FlaskForm
from wtforms import IntegerField, StringField, SubmitField
from wtforms.validators import DataRequired


class AddRecipeForm(FlaskForm):
    name = StringField('Recipe Name', validators=[DataRequired()])
    ingredients_name = StringField('Ingredients Name', validators=[DataRequired()])
    ingredients_amount = IntegerField('Ingredients Amount', validators=[DataRequired()])
    ingredients_unit = StringField('Ingredients Unit', validators=[DataRequired()])
    instructions = StringField('Instructions', validators=[DataRequired()])
    submit = SubmitField('Add')

class SearchForm(FlaskForm):
    ingredient_name = StringField('Ingredient Name', validators=[DataRequired()])
    submit = SubmitField('Search')
