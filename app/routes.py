from app import app, db
from app.forms import AddRecipeForm, SearchForm
from app.models import Ingredients, Recipe, update_recipes
from flask import render_template, redirect, url_for, request


@app.route('/')
@app.route('/index')
def index():
    return render_template('index.html')

@app.route('/recipes', methods=['GET', 'POST'])
def recipes():
    form = AddRecipeForm()
    if form.is_submitted():
        recipe = Recipe(name=form.name.data, instructions=form.instructions.data)
        ingredient = Ingredients(name=form.ingredients_name.data, amount=form.ingredients_amount.data, unit=form.ingredients_unit.data, recipe=recipe)
        db.session.add(recipe)
        db.session.add(ingredient)
        db.session.commit()
        return redirect(url_for('recipes'))
    
    ingredient = request.args.get('ingredient')
    if ingredient:
        #  Not the most efficiency way
        ingredients = Ingredients.query.filter_by(name=ingredient).all()
        recipes = []
        for item in ingredients:
            recipes.append(Recipe.query.get(item.recipe_id))
        if len(recipes) == 0:
            return render_template('404.html', title = '404'), 404
    else:
        recipes = Recipe.query.all()
    return render_template('recipe.html', recipes=recipes, form=form)

@app.route('/recipes/<int:recipe_id>', methods=['GET'])
def recipes_id(recipe_id):
    recipe = Recipe.query.get(recipe_id)
    if not recipe:
        return render_template('404.html', title = '404'), 404
    return render_template('recipe_id.html', recipe=recipe)

@app.route('/search', methods=['GET', 'POST'])
def search():
    form = SearchForm()
    if request.method == 'POST' and form.is_submitted():
        return redirect((url_for('recipes', ingredient=form.ingredient_name.data)))
    return render_template('search.html', form=form)
